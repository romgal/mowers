#/usr/bin/python
# coding: utf-8

"""
Auto-Mower app
"""

__VERSION__ = '1.0'
__LAST_MODIFY_DATE__ = '2019/01/12'

import sys
import argparse
from classes.mower import Mower
from classes.field import Field

def get_version():
    """
    Return a string that displays version of this script
    """
    return "\nVersion: %s (last modified: %s)\n" % (__VERSION__, __LAST_MODIFY_DATE__)

def read_args():
    """
    Parse arguments given by sys
    """
    desc = 'Decription about this remote backup script'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-v", "--version", help="Print version", action='version',\
            version='%s' % (get_version()))
    parser.add_argument("-f", "--file", help="File with initial setup", required=True)
    return parser.parse_args(sys.argv[1:])

def is_next_position_ok(next_position, field, mower):
    """
    A mower can not get outside the field or on a occupied location
    """
    for x, y in zip(next_position, field.size):
        #Trying to get outside the field
        if x > y or x < 0:
            return False
        #A mower get in the way
        for mow in field.mowers:
            if next_position == mow.current_position and mower is not mow:
                return False
    return True

def get_next_position(current_position, orientation, mouv):
    """
    Return the next position
    """
    next_position = list(current_position)
    if orientation == "N":
        if mouv == "F":
            next_position[1] = current_position[1] + 1
        if mouv == "L":
            orientation = "W"
        if mouv == "R":
            orientation = "E"

    elif orientation == "E":
        if mouv == "F":
            next_position[0] = current_position[0] + 1
        if mouv == "L":
            orientation = "N"
        if mouv == "R":
            orientation = "S"

    elif orientation == "W":
        if mouv == "F":
            next_position[0] = current_position[0] - 1
        if mouv == "L":
            orientation = "S"
        if mouv == "R":
            orientation = "N"

    elif orientation == "S":
        if mouv == "F":
            next_position[1] = current_position[1] - 1
        if mouv == "L":
            orientation = "E"
        if mouv == "R":
            orientation = "W"

    return next_position, orientation

def start_mower(mower, field):
    """
    The mower will try to follow its programmed path
	It will refuse to get outside the field or on a occupied location
	   """
    for mouv in mower.path:
        next_position, next_orientation = get_next_position(mower.current_position,
                                                            mower.orientation, mouv)
        if is_next_position_ok(next_position, field, mower):
            mower.current_position = list(next_position)
            mower.orientation = next_orientation

def display_mower(mower):
    """
    Display the current position of a mower
    """
    print ' '.join(str(x) for x in mower.current_position) +  " " + mower.orientation

def initialize_field(setup_file):
    """
    The input file is parsed to initialized the setup
    An exception is raised if the file is not formatted properly
    """
    mowers = []
    try:
        with open(setup_file) as the_file:
            #The first line is extracted
            field_size = tuple(int(x) for x in the_file.readline().replace(" ", "").rstrip())
            liste = the_file.read().splitlines()
            #Lines are read in groups of two and mowers are instanciated
            for position_orientation, path in zip(liste[0::2], liste[1::2]):
                mowers.append(Mower([int(x) for x in position_orientation[:-1].split()],
                                    position_orientation[-1],
                                    path))
            return Field(field_size, mowers)
    except:
        print "Error with the file %s" % setup_file

def main():
    """
    This is the main function
    """
    args = read_args()
    field = initialize_field(args.file)
    for mower in field.mowers:
        start_mower(mower, field)
        display_mower(mower)

if __name__ == '__main__':
    main()
