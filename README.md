
# Auto-Mower
**Please read this before beginning**

## Requirements
* python

## Installation

```
git clone https://bitbucket.org/romgal/mowers.git
```

## Usage

Display the script version

```
python auto-mower.py -v
```

Mow the lawn
```
python auto-mower.py -f file.txt
```
